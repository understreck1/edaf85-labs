import clock.AlarmClockEmulator;
import clock.io.Choice;
import clock.io.ClockInput;
import clock.io.ClockInput.UserInput;
import clock.io.ClockOutput;

public class ClockMain {
    public static void main(String[] args) throws InterruptedException {
        AlarmClockEmulator emulator = new AlarmClockEmulator();

        ClockInput in = emulator.getInput();
        ClockOutput out = emulator.getOutput();

        final var shared = new Shared();
        final var scheduler = new Thread(() -> {
            while(true) {
                final var time = System.currentTimeMillis();
                shared.tick();

                try {
                    Thread.sleep(1000 - time % 1000);
                } catch (InterruptedException e) {
                    System.err.println("Scheduler thread caught an InterruptedException exception:");
                    System.err.println(e.getMessage());
                }
            }
        });
        scheduler.start();

        final var updateClock = new Thread(() -> {
            final var signal = shared.register_get_tick_signal();

            try {
                while (true) {
                    signal.acquire();
                    final var time = shared.get_time();
                    out.displayTime(time.hour, time.minute, time.second);
                }
            } catch (InterruptedException e) {
                System.err.println("Timer thread caught an InterruptedException exception:");
                System.err.println(e.getMessage());
            }
        });
        updateClock.start();

        final var updateAlarm = new Thread(() -> {
            final var signal = shared.register_get_tick_signal();
            var signalsLeft = 0;

            try {
                while (true) {
                        signal.acquire();

                        if(shared.alarm_armed()) {
                            if(shared.get_alarm().equals(shared.get_time())) {
                                signalsLeft = 20;
                            }
                            if(signalsLeft-- > 0) out.alarm();
                        }
                        else signalsLeft = 0;
                    }
                }
            catch (InterruptedException e) {
                System.err.println("Alarm thread caught an InterruptedException exception:");
                System.err.println(e.getMessage());
            }
        });
        updateAlarm.start();

        while (true) {
            in.getSemaphore().acquire();
            UserInput userInput = in.getUserInput();

            Choice c = userInput.choice();
            switch (c) {
                case SET_TIME: {
                    shared.set_time(userInput.hours(), userInput.minutes(), userInput.seconds());
                }
                break;
                case SET_ALARM: {
                    shared.set_alarm(userInput.hours(), userInput.minutes(), userInput.seconds());
                }
                break;
                case TOGGLE_ALARM: {
                    shared.toggle_alarm();
                    out.setAlarmIndicator(shared.alarm_armed());
                }
                break;
                default:
            }
        }
    }
}
