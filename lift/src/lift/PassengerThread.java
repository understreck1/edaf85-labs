package lift;


public class PassengerThread extends Thread {
    private final Monitor m_monitor;
    private final LiftView m_liftView;

    public PassengerThread(Monitor monitor, LiftView liftView) {
        m_monitor = monitor;
        m_liftView = liftView;
    }

    public void run() {
        while (true) {
            try {
                var passenger = m_liftView.createPassenger();
                passenger.begin();

                m_monitor.waitToEnter(passenger.getStartFloor());
                passenger.enterLift();
                m_monitor.enterLift(passenger.getDestinationFloor());

                m_monitor.waitToExit(passenger.getDestinationFloor());
                passenger.exitLift();
                m_monitor.exitLift();

                passenger.end();
            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
            }
        }
    }
}