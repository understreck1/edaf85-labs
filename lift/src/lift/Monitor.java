package lift;


public class Monitor {
    private final int TOP_FLOOR, MAX_PASSENGERS;
    private final int[] m_toEnter;
    private final int[] m_toExit;
    private int m_currentFloor = 0;
    private boolean m_doorsOpen = false;
    private boolean m_goingUp = true;
    private int m_peopleEntering = 0;
    private int m_peopleExiting = 0;

    //initialise monitor
    public Monitor(int nbrFloors, int maxPass) {
        m_toEnter = new int[nbrFloors];
        m_toExit = new int[nbrFloors];

        TOP_FLOOR = nbrFloors - 1;
        MAX_PASSENGERS = maxPass;
    }

    private int to_enter(int from, int to) {
        int k = 0;
        for (int i = from; i <= to; i++) {
            k += m_toEnter[i];
        }

        return k;
    }

    private int to_exit(int from, int to) {
        int k = 0;
        for (int i = from; i <= to; i++) {
            k += m_toExit[i];
        }

        return k;
    }

    public synchronized void close_doors() throws InterruptedException {
        notifyAll();

        while (m_toExit[m_currentFloor] > 0 || m_peopleEntering + m_peopleExiting > 0 ||
                (m_toEnter[m_currentFloor] > 0 && to_exit(0, TOP_FLOOR) < MAX_PASSENGERS)) {
            wait();
        }

        m_doorsOpen = false;
    }

    public synchronized void open_doors() throws InterruptedException {
        m_doorsOpen = true;

        notifyAll();
    }

    // controls movement from one floor to another
    public synchronized int moveLift() throws InterruptedException {
        if (m_goingUp) {
            if (m_currentFloor == TOP_FLOOR ||
                    (to_enter(m_currentFloor + 1, TOP_FLOOR) +
                            to_exit(m_currentFloor + 1, TOP_FLOOR) == 0) ||
                    (to_exit(m_currentFloor + 1, TOP_FLOOR) == 0 && to_exit(0, TOP_FLOOR) == MAX_PASSENGERS)
            ) {
                m_goingUp = false;
            }
        } else {
            if (m_currentFloor == 0 ||
                    (to_enter(0, m_currentFloor - 1) +
                            to_exit(0, m_currentFloor - 1) == 0) ||
                    (to_exit(0, m_currentFloor - 1) == 0 && to_exit(0, TOP_FLOOR) == MAX_PASSENGERS)
            ) {
                m_goingUp = true;
            }
        }

        m_currentFloor = m_goingUp ?
                m_currentFloor + 1 :
                m_currentFloor - 1;

        return m_currentFloor;
    }

    //open lift doors and keep them open while there are people entering or exiting
    public synchronized void waitForPassengers() throws InterruptedException {
        notifyAll();

        while (to_enter(0, TOP_FLOOR) + to_exit(0, TOP_FLOOR) == 0) {
            wait();
        }
    }

    public synchronized boolean shouldStopOnFloor() {
        return m_toExit[m_currentFloor] > 0 ||
                (m_toEnter[m_currentFloor] > 0 && to_exit(0, TOP_FLOOR) < MAX_PASSENGERS);
    }

    public synchronized void waitToEnter(int fromFloor) throws InterruptedException {
        m_toEnter[fromFloor] += 1;
        notifyAll();

        while (m_currentFloor != fromFloor || !m_doorsOpen ||
                to_exit(0, TOP_FLOOR) - m_toExit[m_currentFloor] + m_peopleEntering >= MAX_PASSENGERS) {
            wait();
        }

        m_peopleEntering += 1;
    }

    public synchronized void enterLift(int toFloor) {
        m_toEnter[m_currentFloor] -= 1;
        m_toExit[toFloor] += 1;

        m_peopleEntering -= 1;

        notifyAll();
    }

    public synchronized void waitToExit(int toFloor) throws InterruptedException {
        while (m_currentFloor != toFloor || !m_doorsOpen) {
            wait();
        }

        m_peopleExiting += 1;
    }

    public synchronized void exitLift() {
        m_toExit[m_currentFloor] -= 1;
        m_peopleExiting -= 1;

        notifyAll();
    }

    public synchronized int currentFloor() {
        return m_currentFloor;
    }

    public synchronized boolean areDoorsOpen() {
        return m_doorsOpen;
    }
}