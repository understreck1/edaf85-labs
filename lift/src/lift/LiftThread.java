package lift;

public class LiftThread extends Thread {
    private final Monitor m_monitor;
    private final LiftView m_liftView;

    public LiftThread(Monitor monitor, LiftView liftView) {
        m_monitor = monitor;
        m_liftView = liftView;
    }


    public void run() {
        while (true) {
            try {
                m_monitor.waitForPassengers();
                if (m_monitor.shouldStopOnFloor()) {
                    m_liftView.openDoors(m_monitor.currentFloor());
                    m_monitor.open_doors();
                }

                if (m_monitor.areDoorsOpen()) {
                    m_monitor.close_doors();
                    m_liftView.closeDoors();
                }

                var currentFloor = m_monitor.currentFloor();
                var nextFloor = m_monitor.moveLift();
                m_liftView.moveLift(currentFloor, nextFloor);
            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
            }
        }
    }
}