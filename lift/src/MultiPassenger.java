import lift.LiftThread;
import lift.LiftView;
import lift.Monitor;
import lift.PassengerThread;


public class MultiPassenger {
    public static void main(String[] args) {
        final int NBR_FLOORS = 7, MAX_PASSENGERS = 4;

        var liftView = new LiftView(NBR_FLOORS, MAX_PASSENGERS);
        var monitor = new Monitor(NBR_FLOORS, MAX_PASSENGERS);

        var lift = new LiftThread(monitor, liftView);
        lift.start();

        var passengers = new PassengerThread[20];
        for (var passenger : passengers) {
            passenger = new PassengerThread(monitor, liftView);
            passenger.start();
        }
    }
}