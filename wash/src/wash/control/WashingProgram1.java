package wash.control;

import actor.ActorThread;
import wash.io.WashingIO;

import java.lang.module.Configuration;

import static wash.control.WashingMessage.Order.*;

/**
 * Program 3 for washing machine. This also serves as an example of how washing
 * programs can be structured.
 * 
 * This short program stops all regulation of temperature and water levels,
 * stops the barrel from spinning, and drains the machine of water.
 * 
 * It can be used after an emergency stop (program 0) or a power failure.
 */
public class WashingProgram1 extends ActorThread<WashingMessage> {

    private WashingIO io;
    private ActorThread<WashingMessage> temp;
    private ActorThread<WashingMessage> water;
    private ActorThread<WashingMessage> spin;

    public WashingProgram1(WashingIO io,
                           ActorThread<WashingMessage> temp,
                           ActorThread<WashingMessage> water,
                           ActorThread<WashingMessage> spin) 
    {
        this.io = io;
        this.temp = temp;
        this.water = water;
        this.spin = spin;
    }
    
    @Override
    public void run() {
        try {
            System.out.println("washing program 1 started");

            io.lock(true);

            water.send(new WashingMessage(this, WATER_FILL));
            WashingMessage ack1 = receive();
            System.out.println("got " + ack1);

            temp.send(new WashingMessage(this, TEMP_SET_40));
            WashingMessage ack2 = receive();
            System.out.println("got " + ack2);

            spin.send(new WashingMessage(this, SPIN_SLOW));
            WashingMessage ack3 = receive();
            System.out.println("got " + ack3);

            Thread.sleep(30 * 60000 / Settings.SPEEDUP);

            temp.send(new WashingMessage(this, TEMP_IDLE));
            WashingMessage ack4 = receive();
            System.out.println("got " + ack4);

            water.send(new WashingMessage(this, WATER_DRAIN));
            WashingMessage ack5 = receive();
            System.out.println("got " + ack5);

            for(int i = 0; i < 5; i++) {
                water.send(new WashingMessage(this, WATER_FILL));
                WashingMessage ack6 = receive();
                System.out.println("got " + ack6);

                spin.send(new WashingMessage(this, SPIN_SLOW));
                WashingMessage ack7 = receive();
                System.out.println("got " + ack7);

                Thread.sleep(2 * 60000 / Settings.SPEEDUP);

                water.send(new WashingMessage(this, WATER_DRAIN));
                WashingMessage ack8 = receive();
                System.out.println("got " + ack8);
            }

            spin.send(new WashingMessage(this, SPIN_FAST));
            WashingMessage ack6 = receive();
            System.out.println("got " + ack6);

            Thread.sleep(5 * 60000 / Settings.SPEEDUP);

            spin.send(new WashingMessage(this, SPIN_OFF));
            WashingMessage ack7 = receive();
            System.out.println("got " + ack7);

            water.send(new WashingMessage(this, WATER_IDLE));
            WashingMessage ack8 = receive();
            System.out.println("got " + ack8);

            System.out.println("washing program 1 finished");
        } catch (InterruptedException e) {
            
            // If we end up here, it means the program was interrupt()'ed:
            // set all controllers to idle

            temp.send(new WashingMessage(this, TEMP_IDLE));
            water.send(new WashingMessage(this, WATER_IDLE));
            spin.send(new WashingMessage(this, SPIN_OFF));
            System.out.println("washing program terminated");
        }
    }
}
