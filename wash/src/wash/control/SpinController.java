package wash.control;

import actor.ActorThread;
import wash.io.WashingIO;
import wash.io.WashingIO.Spin;

import java.sql.Time;

public class SpinController extends ActorThread<WashingMessage> {
    private final WashingIO m_io;
    private Spin m_state = Spin.IDLE;

    public SpinController(WashingIO io) {
        m_io = io;
    }

    @Override
    public void run() {
        try {
            while (true) {
                // wait for up to a (simulated) minute for a WashingMessage
                WashingMessage m = receiveWithTimeout(60000 / Settings.SPEEDUP);

                // if m is null, it means a minute passed and no message was received
                if (m != null) {
                    switch (m.order()) {
                        case SPIN_OFF -> {
                            m_state = Spin.IDLE;
                            m.sender().send(new WashingMessage(this, WashingMessage.Order.ACKNOWLEDGMENT));
                        }
                        case SPIN_FAST -> {
                            m_state = Spin.FAST;
                            m.sender().send(new WashingMessage(this, WashingMessage.Order.ACKNOWLEDGMENT));
                        }
                        case SPIN_SLOW -> {
                            m_state = Spin.LEFT;
                            m.sender().send(new WashingMessage(this, WashingMessage.Order.ACKNOWLEDGMENT));
                        }
                        default -> {
                            System.out.println("SpinController got " + m);
                        }
                    }
                }

                switch(m_state) {
                    case FAST -> {
                        if(m_io.getWaterLevel() > 0.0) {
                            m_io.setSpinMode(Spin.IDLE);
                            break;
                        }
                        m_io.setSpinMode(m_state);
                    }
                    case LEFT -> {
                        m_io.setSpinMode(m_state);
                        m_state = Spin.RIGHT;
                    }
                    case RIGHT -> {
                        m_io.setSpinMode(m_state);
                        m_state = Spin.LEFT;
                    }
                    case IDLE -> {
                        m_io.setSpinMode(Spin.IDLE);
                    }
                }
            }
        } catch (InterruptedException unexpected) {
            // we don't expect this thread to be interrupted,
            // so throw an error if it happens anyway
            throw new Error(unexpected);
        }
    }
}
