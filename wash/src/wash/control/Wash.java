package wash.control;

import actor.ActorThread;
import wash.io.WashingIO;
import wash.simulation.WashingSimulator;

public class Wash {

    public static void main(String[] args) throws InterruptedException {
        WashingSimulator sim = new WashingSimulator(Settings.SPEEDUP);

        WashingIO io = sim.startSimulation();

        TemperatureController temp = new TemperatureController(io);
        WaterController water = new WaterController(io);
        SpinController spin = new SpinController(io);

        temp.start();
        water.start();
        spin.start();

        ActorThread<WashingMessage> prog = null;

        while (true) {
            int n = io.awaitButton();
            System.out.println("user selected program " + n);

            switch(n) {
                case 0 -> {
                    if(prog != null) prog.interrupt();
                    prog = new WashingProgram0(io, temp, water, spin);
                }
                case 1 -> {
                    prog = new WashingProgram1(io, temp, water, spin);
                }
                case 2 -> {
                    prog = new WashingProgram2(io, temp, water, spin);
                }
                case 3 -> {
                    prog = new WashingProgram3(io, temp, water, spin);
                }
            }

            prog.start();
        }
    }
};
