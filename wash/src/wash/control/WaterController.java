package wash.control;

import actor.ActorThread;
import wash.io.WashingIO;

public class WaterController extends ActorThread<WashingMessage> {
    private final double WATER_CAPACITY = 20.0;
    private final double WATER_LIMIT = WATER_CAPACITY / 2.0;
    private final double INPUT_FLOW = 0.1;
    private final double OUTPUT_FLOW = 0.2;

    private final int TIMEOUT_MILLISECONDS = 5000;
    private final double TIMEOUT_SECONDS = (double)(TIMEOUT_MILLISECONDS / 1000);

    private final WashingIO m_io;

    private enum Mode {
        IDLE,
        FILLING,
        DRAINING
    }

    private Mode m_mode = Mode.IDLE;
    private WashingMessage m_previousMessage = null;

    public WaterController(WashingIO io) {
        m_io = io;
    }

    @Override
    public void run() {
        try {
            while (true) {
                // wait for up to a (simulated) minute for a WashingMessage
                WashingMessage m = receiveWithTimeout(TIMEOUT_MILLISECONDS / Settings.SPEEDUP);

                // if m is null, it means a minute passed and no message was received
                if (m != null) {
                    switch (m.order()) {
                        case WATER_IDLE -> {
                            m_mode = Mode.IDLE;
                            m_previousMessage = m;
                        }
                        case WATER_FILL -> {
                            m_mode = Mode.FILLING;
                            m_previousMessage = m;
                        }
                        case WATER_DRAIN -> {
                            m_mode = Mode.DRAINING;
                            m_previousMessage = m;
                        }
                        default -> {
                            System.out.println("SpinController got " + m);
                        }
                    }
                }

                switch(m_mode) {
                    case IDLE -> {
                        m_io.fill(false);
                        m_io.drain(false);
                        if(m_previousMessage != null) {
                            m_previousMessage.sender().send(new WashingMessage(this, WashingMessage.Order.ACKNOWLEDGMENT));
                        }
                    }
                    case FILLING -> {
                        m_io.drain( false);
                        if(m_io.getWaterLevel() > WATER_LIMIT) {
                            m_io.fill(false);
                            m_mode = Mode.IDLE;
                            m_previousMessage.sender().send(new WashingMessage(this, WashingMessage.Order.ACKNOWLEDGMENT));
                            m_previousMessage = null;
                        }
                        else {
                            m_io.fill(true);
                        }
                    }
                    case DRAINING -> {
                        m_io.fill(false);
                        m_io.drain(true);
                        if(m_io.getWaterLevel() == 0 && m_previousMessage != null) {
                            m_previousMessage.sender().send(new WashingMessage(this, WashingMessage.Order.ACKNOWLEDGMENT));
                            m_previousMessage = null;
                        }
                    }
                }
            }
        } catch (InterruptedException unexpected) {
            // we don't expect this thread to be interrupted,
            // so throw an error if it happens anyway
            throw new Error(unexpected);
        }
    }
}
