package wash.control;

import actor.ActorThread;
import wash.io.WashingIO;
import wash.control.WashingMessage;

public class TemperatureController extends ActorThread<WashingMessage> {
    private final double HEATING_MARGIN = 0.478 + 0.2;
    private final double COOLING_MARGIN = 2 - (9.52 / 100 + 0.2);
    private final double COOLER_TEMP = 40;
    private final double HOTTER_TEMP = 60;

    private WashingIO m_io;

    private enum Mode {
        IDLE,
        HEATING,
        WAITING
    };
    private Mode m_mode = Mode.IDLE;
    private double m_targetTemp = COOLER_TEMP;
    private WashingMessage m_previousMessage;

    public TemperatureController(WashingIO io) {
        m_io = io;
    }

    @Override
    public void run() {
        try {
            while (true) {
                // wait for up to a (simulated) minute for a WashingMessage
                WashingMessage m = receiveWithTimeout(10000 / Settings.SPEEDUP);

                if(m != null) {
                    switch(m.order()) {
                        case TEMP_IDLE -> {
                            m_mode = Mode.IDLE;
                            m_previousMessage = m;
                        }
                        case TEMP_SET_40 -> {
                            m_mode = Mode.HEATING;
                            m_targetTemp = COOLER_TEMP;
                            m_previousMessage = m;
                        }
                        case TEMP_SET_60 -> {
                            m_mode = Mode.HEATING;
                            m_targetTemp = HOTTER_TEMP;
                            m_previousMessage = m;
                        }
                        default -> {
                            System.out.println("TemperatureController got " + m);
                        }
                    }
                }

                switch(m_mode) {
                    case IDLE -> {
                        m_io.heat(false);
                        if(m_previousMessage != null) {
                            m_previousMessage.sender().send(new WashingMessage(this, WashingMessage.Order.ACKNOWLEDGMENT));
                            m_previousMessage = null;
                        }
                    }
                    case HEATING -> {
                        if(m_io.getWaterLevel() == 0.0) {
                            m_io.heat(false);
                            break;
                        }

                        var temp = m_io.getTemperature();
                        if(temp + HEATING_MARGIN < m_targetTemp) {
                            m_io.heat(true);
                        }
                        else {
                            m_io.heat(false);
                            if(m_previousMessage != null) {
                                m_previousMessage.sender().send(new WashingMessage(this, WashingMessage.Order.ACKNOWLEDGMENT));
                                m_previousMessage = null;
                            }
                            m_mode = Mode.WAITING;
                        }
                    }
                    case WAITING -> {
                        var temp = m_io.getTemperature();
                        if(temp > (m_targetTemp - COOLING_MARGIN)) {
                            m_io.heat(false);
                        }
                        else {
                            if(m_io.getWaterLevel() > 0.0) {
                                m_io.heat(true);
                            }
                            else {
                                m_io.heat(false);
                            }
                            m_mode = Mode.HEATING;
                        }
                    }
                }
            }
        } catch (InterruptedException unexpected) {
            // we don't expect this thread to be interrupted,
            // so throw an error if it happens anyway
            throw new Error(unexpected);
        }
    }
}
